package vn.name.hohoanghai.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static vn.name.hohoanghai.utils.Constant.BASE_URL;

/**
 * Thực hiện các thao tác với SharedPreferences
 * Created by hhhai0304@gmail.com on 05/01/2017.
 */

public class SessionUtil {
    private final String keyMSSV = "mssv";
    private final String keyHomePage = "homepage";
    private final String opened = "opened";

    private SharedPreferences preferences;

    public SessionUtil(Activity context) {
        preferences = context.getSharedPreferences("setting", MODE_PRIVATE);
    }

    public void createSession(String mssv) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(keyMSSV, mssv);
        editor.apply();
    }

    public String getMaSinhVien() {
        return preferences.getString(keyMSSV, "");
    }

    public void clearSession() {
        preferences.edit().remove(keyMSSV).apply();
    }

    public boolean alreadyLoggedIn() {
        return !preferences.getString(keyMSSV, "").isEmpty();
    }

    public String getHomePage() {
        return preferences.getString(keyHomePage, "");
    }

    public String getHomePage(String maSinhVien) {
        String homePage = preferences.getString(keyHomePage, "");
        if (homePage.isEmpty()) {
            return BASE_URL + "Default.aspx";
        } else if (homePage.equals(BASE_URL + "Default.aspx")) {
            return homePage;
        } else {
            return  homePage + maSinhVien;
        }
    }

    public void setHomePage(String homePage) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(keyHomePage, homePage);
        editor.apply();
    }

    public void setOpened() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(opened, true);
        editor.apply();
    }

    public boolean isOpened() {
        return preferences.getBoolean(opened, false);
    }
}