package vn.name.hohoanghai.fragments;

import android.Manifest;
import android.app.DownloadManager;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.name.hohoanghai.pdtntt.PdfViewerActivity;
import vn.name.hohoanghai.pdtntt.R;

/**
 * Màn hình chính chứa WebView để xử lý load web Phòng Đào Tạo
 * Created by hhhai0304@gmail.com on 30-06-2016.
 */

public class MainFragment extends Fragment {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btnRefresh)
    FloatingActionButton btnRefresh;
    @BindView(R.id.mainFragment)
    FrameLayout mainFragment;

    String maSinhVien, url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        url = getArguments().getString("url");
        maSinhVien = getArguments().getString("maSinhVien");

        loadWebView();
        return view;
    }

    private void loadWebView() {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setUseWideViewPort(true);

        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:(function(){document.getElementById('ctl00_ucRight1_txtMaSV').value='" + maSinhVien + "';})()");
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                webView.setVisibility(View.INVISIBLE);
                makeNotice(mainFragment, getString(R.string.no_internet));
            }

            @Override
            public void onPageStarted(WebView view, String thisUrl, Bitmap favicon) {
                super.onPageStarted(view, thisUrl, favicon);
                if (thisUrl.endsWith(".pdf") || thisUrl.endsWith(".rar") || thisUrl.endsWith(".doc") || thisUrl.endsWith(".docx") || thisUrl.endsWith(".xlsx") || thisUrl.endsWith(".xls")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            downloadFile(thisUrl);
                        } else {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }
                    } else {
                        downloadFile(thisUrl);
                    }
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });

        webView.setInitialScale(100);
        webView.loadUrl(url);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();
            }
        });
    }

    private void downloadFile(String url) {
        final String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + URLUtil.guessFileName(url, null, null);
        if (new File(filePath).exists() && filePath.endsWith(".pdf")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getString(R.string.thong_bao));
            builder.setMessage(getString(R.string.file_exist));
            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intentView = new Intent(getActivity(), PdfViewerActivity.class);
                    intentView.putExtra("PDF", filePath);
                    startActivity(intentView);
                }
            });
            builder.setNegativeButton(getString(R.string.no), null);
            builder.create().show();
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager.getActiveNetworkInfo() == null) {
                makeNotice(mainFragment, getString(R.string.no_internet));
                return;
            }

            if (!new File(Environment.DIRECTORY_DOWNLOADS).isDirectory()) {
                new File(Environment.DIRECTORY_DOWNLOADS).mkdirs();
            }

            DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    downloadUri.getLastPathSegment());
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            downloadManager.enqueue(request);
        }
    }

    private void makeNotice(View view, String notice) {
        if (view == null) {
            Toast.makeText(getActivity(), notice, Toast.LENGTH_SHORT).show();
        } else {
            Snackbar.make(view, notice, Snackbar.LENGTH_SHORT).show();
        }
    }
}