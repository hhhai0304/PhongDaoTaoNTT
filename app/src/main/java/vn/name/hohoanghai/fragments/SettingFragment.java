package vn.name.hohoanghai.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.name.hohoanghai.pdtntt.R;
import vn.name.hohoanghai.utils.SessionUtil;

import static vn.name.hohoanghai.utils.Constant.BASE_URL;

/**
 * Màn hình "Cài Đặt"
 * Created by hhhai0304@gmail.com on 30-06-2016.
 */

public class SettingFragment extends Fragment {

    @BindView(R.id.rbTrangChu)
    RadioButton rbTrangChu;
    @BindView(R.id.rbLichHocTheoTuan)
    RadioButton rbLichHocTheoTuan;
    @BindView(R.id.rbLichHoc)
    RadioButton rbLichHoc;
    @BindView(R.id.rbLichThi)
    RadioButton rbLichThi;
    @BindView(R.id.rbKetQuaHocTap)
    RadioButton rbKetQuaHocTap;
    @BindView(R.id.rbgSetting)
    RadioGroup rbgSetting;

    SessionUtil sessionUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);

        sessionUtil = new SessionUtil(getActivity());
        rbgSetting.setOnCheckedChangeListener(null);
        String homePage = sessionUtil.getHomePage();

        // Kiểm tra xem đang chọn Home Page là gì để checked nó trên giao diện
        switch (homePage) {
            case BASE_URL + "Default.aspx":
                rbTrangChu.setChecked(true);
                break;
            case BASE_URL + "LichHocLichThiTuan.aspx?MSSV=":
                rbLichHocTheoTuan.setChecked(true);
                break;
            case BASE_URL + "XemLichHoc.aspx?MSSV=":
                rbLichHoc.setChecked(true);
                break;
            case BASE_URL + "XemLichThi.aspx?MSSV=":
                rbLichThi.setChecked(true);
                break;
            case BASE_URL + "XemDiem.aspx?MSSV=":
                rbKetQuaHocTap.setChecked(true);
                break;
            default:
                rbTrangChu.setChecked(true);
                break;
        }

        rbgSetting.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                String homePage = "";
                if (rbTrangChu.isChecked()) {
                    sessionUtil.setHomePage(BASE_URL + "Default.aspx");
                    homePage = "Trang chủ Phòng Đào tạo";
                } else if (rbLichHocTheoTuan.isChecked()) {
                    sessionUtil.setHomePage(BASE_URL + "LichHocLichThiTuan.aspx?MSSV=");
                    homePage = "Trang xem Lịch học theo tuần";
                } else if (rbLichHoc.isChecked()) {
                    sessionUtil.setHomePage(BASE_URL + "XemLichHoc.aspx?MSSV=");
                    homePage = "Trang xem Lịch học";
                } else if (rbLichThi.isChecked()) {
                    sessionUtil.setHomePage(BASE_URL + "XemLichThi.aspx?MSSV=");
                    homePage = "Trang xem Lịch thi";
                } else if (rbKetQuaHocTap.isChecked()) {
                    sessionUtil.setHomePage(BASE_URL + "XemDiem.aspx?MSSV=");
                    homePage = "Trang xem Kết quả học tập";
                }
                Toast.makeText(getActivity(), "Đã chọn màn hình khởi động là " + homePage, Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}