package vn.name.hohoanghai.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.name.hohoanghai.pdtntt.R;

/**
 * Màn hình "Quy định phân tiết học"
 * Created by hhhai0304@gmail.com on 30-06-2016.
 */

public class TietHocFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tiethoc, container, false);
    }
}