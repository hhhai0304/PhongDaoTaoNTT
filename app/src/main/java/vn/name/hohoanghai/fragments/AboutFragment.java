package vn.name.hohoanghai.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.name.hohoanghai.pdtntt.R;

/**
 * Màn hình "Giới Thiệu"
 * Created by hhhai0304@gmail.com on 30-06-2016.
 */

public class AboutFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void openWebBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @OnClick({R.id.mySite, R.id.tvPhongDaoTao})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mySite:
                openWebBrowser("https://www.facebook.com/hhhai0304");
                break;
            case R.id.tvPhongDaoTao:
                openWebBrowser("http://phongdaotao.ntt.edu.vn/");
                break;
        }
    }
}