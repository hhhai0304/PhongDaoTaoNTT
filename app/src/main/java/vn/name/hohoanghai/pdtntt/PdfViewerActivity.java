package vn.name.hohoanghai.pdtntt;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PdfViewerActivity extends AppCompatActivity {

    @BindView(R.id.pdf_view)
    PDFView pdfView;

    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().hide();

        getData();
    }

    private void getData() {
        file = new File(getIntent().getStringExtra("PDF").replace("%20", " ").replace("file://", ""));
        pdfView.fromUri(Uri.fromFile(file)).load();
    }

    @Override
    public void onBackPressed() {
        try {
            file.delete();
        } catch (Exception e) {
            throw new RuntimeException("Không thể delete file", e);
        } finally {
            super.onBackPressed();
        }
    }
}