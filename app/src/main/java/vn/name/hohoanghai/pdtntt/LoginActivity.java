package vn.name.hohoanghai.pdtntt;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.name.hohoanghai.utils.SessionUtil;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edtMSSV)
    EditText edtMSSV;

    SessionUtil sessionUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        initializeViews();
        initializeEvents();
        checkLogin();
    }

    private void initializeViews() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        edtMSSV.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        edtMSSV.setTypeface(Typeface.SERIF);

        sessionUtil = new SessionUtil(this);
    }

    private void initializeEvents() {
        edtMSSV.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    login();
                    return true;
                }
                return false;
            }
        });
    }

    private void login() {
        String maSinhVien = edtMSSV.getText().toString();
        if (maSinhVien.length() == 10) {
            sessionUtil.createSession(maSinhVien);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            edtMSSV.setText("");
            edtMSSV.requestFocus();
            Toast.makeText(this, getString(R.string.incorrect_mssv), Toast.LENGTH_SHORT).show();
        }
    }

    private void checkLogin() {
        if (sessionUtil.alreadyLoggedIn()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.btnLogin)
    public void onClick() {
        login();
    }
}