package vn.name.hohoanghai.pdtntt;

import android.app.DownloadManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import vn.name.hohoanghai.fragments.AboutFragment;
import vn.name.hohoanghai.fragments.MainFragment;
import vn.name.hohoanghai.fragments.SettingFragment;
import vn.name.hohoanghai.fragments.TietHocFragment;
import vn.name.hohoanghai.utils.SessionUtil;

import static vn.name.hohoanghai.utils.Constant.BASE_URL;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mainActivity)
    LinearLayout mainActivity;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.adView)
    AdView adView;

    View headerView;
    LinearLayout header;
    ImageView imgAvatar;
    TextView tvMSSV;

    String maSinhVien;
    long backPressed;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    SessionUtil sessionUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MobileAds.initialize(this, "ca-app-pub-9204613093493847~6172734415");

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        initializeViews();
        initializeEvents();
    }

    private void initializeViews() {
        sessionUtil = new SessionUtil(this);

        if (!sessionUtil.isOpened()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(getString(R.string.title_thong_bao_update));
            builder.setMessage(getString(R.string.thong_bao_update));
            builder.setPositiveButton(getString(R.string.seen), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sessionUtil.setOpened();
                }
            });
            builder.setCancelable(false);
            builder.create().show();
        }

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        fragmentManager = getFragmentManager();

        headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        header = (LinearLayout) headerView.findViewById(R.id.navHeader);
        imgAvatar = (ImageView) headerView.findViewById(R.id.imgAvatar);
        tvMSSV = (TextView) headerView.findViewById(R.id.tvMaSinhVien);
        mainActivity = (LinearLayout) headerView.findViewById(R.id.mainActivity);

        maSinhVien = sessionUtil.getMaSinhVien();

        tvMSSV.setText(maSinhVien);
        setUserAvatar(BASE_URL + "GetImage.aspx?MSSV=" + maSinhVien);

        Crashlytics.setUserIdentifier(maSinhVien);
    }

    private void initializeEvents() {
        replaceFragment(createMainFragment(sessionUtil.getHomePage(maSinhVien)));
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        onDownloadCompleted();
    }

    private void logout() {
        sessionUtil.clearSession();

        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void setUserAvatar(String url) {
        Glide.with(this).load(url).asBitmap().centerCrop()
                .error(R.drawable.ic_user).into(new BitmapImageViewTarget(imgAvatar) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgAvatar.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    private MainFragment createMainFragment(String url) {
        MainFragment mainFragment = new MainFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("maSinhVien", maSinhVien);
        mainFragment.setArguments(bundle);
        return mainFragment;
    }

    private void replaceFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainActivity, fragment);
        fragmentTransaction.commit();
    }

    private void onDownloadCompleted() {
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long id_reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(id_reference);

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

                Cursor cursor = downloadManager.query(query);
                if (cursor.moveToFirst()) {
                    final String path = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(getString(R.string.thong_bao));

                    if (path != null && path.endsWith(".pdf")) {
                        builder.setMessage(getString(R.string.download_completed));
                        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intentView = new Intent(MainActivity.this, PdfViewerActivity.class);
                                intentView.putExtra("PDF", path);
                                startActivity(intentView);
                            }
                        });
                        builder.setNegativeButton(getString(R.string.no), null);
                    } else if (path != null && !path.endsWith(".pdf")) {
                        builder.setMessage(getString(R.string.download_completed_with_path, path.replace("file:///", "").replace("%20", " ")));
                        builder.setPositiveButton(getString(android.R.string.ok), null);
                    }
                    builder.create().show();
                }
            }
        };
        registerReceiver(receiver, filter);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = fragmentManager.findFragmentById(R.id.mainActivity);

            if (f instanceof AboutFragment || f instanceof SettingFragment) {
                replaceFragment(createMainFragment(BASE_URL + "Default.aspx"));
                setTitle(getString(R.string.app_name));
            } else {
                if (backPressed + 2000 > System.currentTimeMillis()) {
                    System.exit(0);
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.click_to_exit), Toast.LENGTH_SHORT).show();
                }
                backPressed = System.currentTimeMillis();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                replaceFragment(createMainFragment(BASE_URL + "Default.aspx"));
                setTitle(getString(R.string.app_name));
                break;
            case R.id.nav_lichhoctheotuan:
                replaceFragment(createMainFragment(BASE_URL + "LichHocLichThiTuan.aspx?MSSV=" + maSinhVien));
                setTitle(getString(R.string.lich_hoc_theo_tuan));
                break;
            case R.id.nav_lichhoc:
                replaceFragment(createMainFragment(BASE_URL + "XemLichHoc.aspx?MSSV=" + maSinhVien));
                showNoticeDialog(getString(R.string.luu_y_lich_hoc));
                setTitle(getString(R.string.lich_hoc));
                break;
            case R.id.nav_lichthi:
                replaceFragment(createMainFragment(BASE_URL + "XemLichThi.aspx?MSSV=" + maSinhVien));
                showNoticeDialog(getString(R.string.luu_y_lich_thi));
                setTitle(getString(R.string.lich_thi));
                break;
            case R.id.nav_ketquahoctap:
                replaceFragment(createMainFragment(BASE_URL + "XemDiem.aspx?MSSV=" + maSinhVien));
                setTitle(getString(R.string.ket_qua_hoc_tap));
                break;
            case R.id._nav_diemdanh:
                replaceFragment(createMainFragment(BASE_URL + "ThongTinDiemDanh.aspx?MSSV=" + maSinhVien));
                showNoticeDialog(getString(R.string.luu_y_diem_danh));
                setTitle(getString(R.string.diem_danh));
                break;
            case R.id.nav_congno:
                replaceFragment(createMainFragment(BASE_URL + "CongNoSinhVien.aspx?MSSV=" + maSinhVien));
                setTitle(getString(R.string.cong_no));
                break;
            case R.id.nav_chuandaura:
                replaceFragment(createMainFragment(BASE_URL + "News.aspx?MenuID=351"));
                setTitle(getString(R.string.chuan_dau_ra));
                break;
            case R.id.nav_tiethoc:
                replaceFragment(new TietHocFragment());
                setTitle(getString(R.string.quy_dinh_phan_tiet_hoc));
                break;
            case R.id.nav_vitriphonghoc:
                replaceFragment(createMainFragment(BASE_URL + "NewsDetail.aspx?NewsID=580"));
                setTitle(getString(R.string.vi_tri));
                break;
            case R.id.nav_donghocphi:
                replaceFragment(createMainFragment(BASE_URL + "NewsDetail.aspx?NewsID=474"));
                setTitle(getString(R.string.dong_hoc_phi));
                break;
            case R.id.nav_caidat:
                replaceFragment(new SettingFragment());
                setTitle(getString(R.string.cai_dat));
                break;
            case R.id.nav_gioithieu:
                replaceFragment(new AboutFragment());
                setTitle(getString(R.string.gioi_thieu));
                break;
            default:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showNoticeDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.luu_y));
        builder.setMessage(message);
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }
}